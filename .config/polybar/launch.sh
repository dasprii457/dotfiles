#!/usr/bin/env bash

# gruvbox colorscheme
#export cpuLabel="%{B#8ec07c}%{F#1d2021} CPU %{B- F-}%{B#504945}%{F#ebdbb2} %percentage%% %{B- F-}"
#export tempLabel="%{B#83a598}%{F#1d2021} TEMP %{B- F-}%{B#504945}%{F#ebdbb2} %temperature-c% %{B- F-}"
#export memLabel="%{B#fabd2f}%{F#1d2021} MEM %{B- F-}%{B#504945}%{F#ebdbb2} %gb_used% %{B- F-}"
#export dateLabel="%{B#fe8019}%{F#1d2021}  %{B- F-}%{B#504945}%{F#ebdbb2} %m/%d/%y %{B- F-}"
#export dateLabel_alt="%{B#fe8019}%{F#1d2021}  %{B- F-}%{B#504945}%{F#ebdbb2} %A, %B %d, %Y %{B- F-}"
#export timeLabel="%{B#d3869b}%{F#1d2021} 󱑆 %{B- F-}%{B#504945}%{F#ebdbb2} %I:%M %p %{B- F-}"

# nord colorscheme
export cpuLabel="%{B#a3be8c}%{F#2e3440} CPU %{B- F-}%{B#4c566a}%{F#eceff4} %percentage%% %{B- F-}"
export tempLabel="%{B#8fbcbb}%{F#2e3440} TEMP %{B- F-}%{B#4c566a}%{F#eceff4} %temperature-c% %{B- F-}"
export memLabel="%{B#ebcb8b}%{F#2e3440} MEM %{B- F-}%{B#4c566a}%{F#eceff4} %gb_used% %{B- F-}"
export dateLabel="%{B#d08770}%{F#2e3440}  %{B- F-}%{B#4c566a}%{F#eceff4} %m/%d/%y %{B- F-}"
export dateLabel_alt="%{B#d08770}%{F#2e3440}  %{B- F-}%{B#4c566a}%{F#eceff4} %A, %B %d, %Y %{B- F-}"
export timeLabel="%{B#b48ead}%{F#2e3440} 󱑆 %{B- F-}%{B#4c566a}%{F#eceff4} %I:%M %p %{B- F-}"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch bar1, bar2, and bar3
echo "Launching..." | tee -a /tmp/polybar1.log /tmp/polybar2.log /tmp/polybar3.log
polybar one >>/tmp/polybar1.log 2>&1 &
polybar two >>/tmp/polybar2.log 2>&1 &
#polybar three >>/tmp/polybar3.log 2>&1 &
echo "Bar(s) launched!"
