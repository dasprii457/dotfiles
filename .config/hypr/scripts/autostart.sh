#!/bin/bash
# check for existing PID before executing command
function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run hypridle
run agsv1
run pypr
run linux-wallpaperengine --screen-root DP-1 --screen-root DP-2 --silent 1924444502
run lxpolkit
run vesktop
run tidal-hifi
run easyeffects --gapplication-service
run otd-daemon
run mpDris2
sleep 1s; run gpu-screen-recorder -w DP-1 -c mp4 -f 60 -a "default_output|easyeffects_source"\
  -q ultra -r 90 -k h264 -ac opus -o "$HOME/Videos/Clips" -v no
run steam
run kitty -e btop
