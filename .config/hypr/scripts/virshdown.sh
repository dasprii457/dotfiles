#!/bin/bash -e
virsh list --all | grep -E 'running' | awk '{print $2}' | xargs -t -I {} virsh shutdown {}
