/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 0;       /* snap pixel */
static const unsigned int gappih    = 10;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 10;       /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int usealtbar          = 1;        /* 1 means use non-dwm status bar */
static const char *altbarclass      = "Polybar"; /* Alternate bar class name */
static const char *alttrayname      = "tray";    /* Polybar tray instance name */
static const char *altbarcmd        = "$HOME/.config/polybar/launch.sh"; /* Alternate bar launch command */
static const char *fonts[]          = { "Ubuntu Mono:size=10" } /* Placeholder font. Actual font selection 
                                                                   will be handled by polybar.*/

#include "themes/gruv.h"

/* tagging */
static const char *tags[] = { "󰎤", "󰎧", "󰎪", "󰎭", "󰎱", "󰎳", "󰎶", "󰎹", "󰎼" };

static const Rule rules[] = {
  /* xprop(1):
   *  WM_CLASS(STRING) = instance, class
   *  WM_NAME(STRING) = title
   */
  /* class                instance    title       tags mask     iscentered    isfloating    monitor */
  { "Lxappearance",       0,          0,          0,            1,            1,            1 },
  { "Pavucontrol",        0,          0,          0,            1,            1,            1 },
  { "Virtualbox Manager", 0,          0,          0,            1,            1,            1 },
  { "Lutris",             0,          0,          0,            1,            1,            1 },
  { "Wine",               0,          0,          0,            1,            1,            1 },
  { "Thunar",             0,          0,          0,            1,            1,            1 },
  { "Blueman-manager",    0,          0,          0,            1,            1,            1 },
  { "feh",                0,          0,          0,            1,            1,            1 },
  { "Lxpolkit",           0,          0,          0,            1,            1,            1 },
  { "Timeshift-gtk",      0,          0,          0,            1,            1,            1 },
  { "cemu.exe",           0,          0,          0,            1,            1,            1 },
  { "origin.exe",         0,          0,          0,            1,            1,            1 },
  { "Origin",             0,          0,          0,            1,            1,            1 },
  { "explorer.exe",       0,          0,          0,            1,            1,            1 },
  { "steam",              0,          0,          0,            1,            1,            1 },
  { "battle.net.exe",     0,          0,          0,            1,            1,            1 },
  { "Pamac-manager",      0,          0,          0,            1,            1,            1 },
  { "Galculator",         0,          0,          0,            1,            1,            1 },
  { "XIVLauncher.Core",   0,          0,          0,            1,            1,            1 },
  { "ProtonUp-Qt",        0,          0,          0,            1,            1,            1 },
  { "Yas",                0,          "SteamTinkerLaunch", 0,   1,            1,            1 },
  /* monitor 2 */
  { "discord",            0,          0,          0,            1,            0,            -1 },
  { "Spotify",            0,          "-",        0,            1,            0,            -1 },
  { "Spotify",            0,          "Spotify",  0,            1,            0,            -1 },
  { "kitty",              0,          "gotop",    0,            1,            0,            -1 },
  { "kitty",              0,          "btop",     0,            1,            0,            -1 },
  { "Deadbeef",           0,          0,          0,            1,            0,            -1 }
};


/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int attachbelow = 1;    /* 1 means attach after the currently active window */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile }     /* first entry is default */
	//{ "><>",      NULL },    /* no layout function means floating behavior */
	//{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* XF86 key definitions for media key assignments */
#include <X11/XF86keysym.h>

/* movestack patch */
#include "movestack.c"

/* keybinds */
static Key keys[] = {
  /* modifier                     key                 function        argument */

  // media keys
  { 0,                            XF86XK_AudioPlay,   spawn,          SHCMD("playerctl play-pause") },
  { 0,                            XF86XK_AudioNext,   spawn,          SHCMD("playerctl next") },
  { 0,                            XF86XK_AudioPrev,   spawn,          SHCMD("playerctl previous") },

  // screenshot tools
  { 0,                            XK_Print,           spawn,          SHCMD("flameshot gui") },
  { ShiftMask,                    XK_Print,           spawn,          SHCMD("scrot -a 0,0,2560,1440") },

  // start rofi (a program launcher)
  //{ MODKEY,                       XK_d,               spawn,          SHCMD("/home/dasprii/.config/rofi.dwm/scripts/launcher_t1") },
  { MODKEY,                       XK_d,               spawn,          SHCMD("rofi -show drun") },

  // start kitty (a terminal)
  { MODKEY,                       XK_Return,          spawn,          SHCMD("kitty") },

  // binds to start and kill picom (an X11 compositor)
  { MODKEY,                       XK_p,               spawn,          SHCMD("picom -b") },
  { MODKEY|ShiftMask,             XK_p,               spawn,          SHCMD("killall picom") },

  // change mfact sizes
  { MODKEY,                       XK_h,               setmfact,       {.f = -0.05} },
  { MODKEY,                       XK_l,               setmfact,       {.f = +0.05} },

  // window control
  { MODKEY,                       XK_j,               focusstack,     {.i = +1 } },
  { MODKEY,                       XK_k,               focusstack,     {.i = -1 } },
  { MODKEY|ShiftMask,             XK_j,               movestack,      {.i = +1 } },
  { MODKEY|ShiftMask,             XK_k,               movestack,      {.i = -1 } },
  { MODKEY|ShiftMask,             XK_space,           togglefloating, {0} },
  { MODKEY,                       XK_f,               togglefullscr,  {0} },

  // gaps
  { MODKEY,                       XK_minus,           incrgaps,       {.i = -5 } },
  { MODKEY,                       XK_equal,           incrgaps,       {.i = +5 } },
  { MODKEY|ShiftMask,             XK_minus,           togglegaps,     {0} },
  { MODKEY|ShiftMask,             XK_equal,           defaultgaps,    {0} },

  // layout
  { MODKEY,                       XK_space,           setlayout,      {0} },
  { MODKEY,                       XK_comma,           focusmon,       {.i = -1 } },
  { MODKEY,                       XK_period,          focusmon,       {.i = +1 } },
  { MODKEY|ShiftMask,             XK_comma,           tagmon,         {.i = -1 } },
  { MODKEY|ShiftMask,             XK_period,          tagmon,         {.i = +1 } },
  { MODKEY,                       XK_Tab,             layoutscroll,   {.i = +1 } },
  { MODKEY|ShiftMask,             XK_Tab,             layoutscroll,   {.i = -1 } },
  { MODKEY,                       XK_b,               togglebar,      {0} },

  // kill dwm
  { MODKEY|ShiftMask,             XK_e,               quit,           {0} },

  // kill window
  { MODKEY|ShiftMask,             XK_q,               killclient,     {0} },

  // restart dwm
  { MODKEY|ShiftMask,             XK_r,               quit,           {1} },

  TAGKEYS(                        XK_1,                                0)
  TAGKEYS(                        XK_2,                                1)
  TAGKEYS(                        XK_3,                                2)
  TAGKEYS(                        XK_4,                                3)
  TAGKEYS(                        XK_5,                                4)
  TAGKEYS(                        XK_6,                                5)
  TAGKEYS(                        XK_7,                                6)
  TAGKEYS(                        XK_8,                                7)
  TAGKEYS(                        XK_9,                                8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

static const char *ipcsockpath = "/tmp/dwm.sock";
static IPCCommand ipccommands[] = {
  IPCCOMMAND(  view,                1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggleview,          1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tag,                 1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  toggletag,           1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  tagmon,              1,      {ARG_TYPE_UINT}   ),
  IPCCOMMAND(  focusmon,            1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  focusstack,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  zoom,                1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  incnmaster,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  killclient,          1,      {ARG_TYPE_SINT}   ),
  IPCCOMMAND(  togglefloating,      1,      {ARG_TYPE_NONE}   ),
  IPCCOMMAND(  setmfact,            1,      {ARG_TYPE_FLOAT}  ),
  IPCCOMMAND(  setlayoutsafe,       1,      {ARG_TYPE_PTR}    ),
  IPCCOMMAND(  quit,                1,      {ARG_TYPE_NONE}   )
};
