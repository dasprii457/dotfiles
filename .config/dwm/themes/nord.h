// nord colors
;static const char *colors[][3] = {
  /*                fg          bg          border */
  [SchemeNorm]  = { "#8fbcbb",  "#4c566a",  "#4c566a" },
  [SchemeSel]   = { "#4c566a",  "#8fbcbb",  "#8fbcbb" },
};

