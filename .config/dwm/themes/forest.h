// color palette
static const char bg1[]     = "#4c5866";
static const char bg2[]     = "#3c4c55";
static const char border[]  = "#a9dd9d"; // bright green
static const char tag1[]    = "#84a78d"; // dark green
static const char tag2[]    = "#bdd0e5"; // bright blue
static const char tag3[]    = "#daccf0"; // magenta
static const char tag4[]    = "#fd888d"; // bright red 
static const char tag5[]    = "#eed094"; // yellow
static const char tag6[]    = "#a9dd9d"; // bright green
static const char tag7[]    = "#bdd0e5"; // bright blue
static const char tag8[]    = "#daccf0"; // magenta
static const char tag9[]    = "#fd888d"; // bright red

// wm colors
static const char *colors[][3] = {
  /*                fg    bg      border */
  [SchemeNorm]  = { bg2,  bg1,    bg1    },
  [SchemeSel]   = { bg1,  border, border },
};

// tag colors
static const char *tagsel[][2] = {
 /* fg    bg  */
  { tag1, bg1 },
  { tag2, bg1 },
  { tag3, bg1 },
  { tag4, bg1 },
  { tag5, bg1 },
  { tag6, bg1 },
  { tag7, bg1 },
  { tag8, bg1 },
  { tag9, bg1 },
};
