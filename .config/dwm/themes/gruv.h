// gruvbox dark colors
;static const char *colors[][3] = {
  /*                fg          bg            border */
  [SchemeNorm]  = { "#ebdbb2",  "#504945",    "#504945" },
  [SchemeSel]   = { "#504945",  "#ebdbb2",    "#ebdbb2" },
};

