// color pallete
static const char bg1[]	    = "#2e3440";
static const char bg2[]	    = "#4c566a";
static const char border[]  = "#8fbcbb";
static const char tag1[]    = "#8fbcbb";
static const char tag2[]    = "#bf616a";
static const char tag3[]    = "#88c0d0";
static const char tag4[]    = "#d08770";
static const char tag5[]    = "#81a1c1";
static const char tag6[]    = "#ebcb8b";
static const char tag7[]    = "#5e81ac";
static const char tag8[]    = "#a3be8c";
static const char tag9[]    = "#b48ead";

// wm colors
static const char *colors[][3] = {
  /*                fg    bg      border */
  [SchemeNorm]  = { bg2,  bg1,    bg2    },
  [SchemeSel]   = { bg1,  border, border },
};

// tag colors
static const char *tagsel[][2] = {
 /* fg      bg     */
  { tag1, bg1 },
  { tag2, bg1 },
  { tag3, bg1 },
  { tag4, bg1 },
  { tag5, bg1 },
  { tag6, bg1 },
  { tag7, bg1 },
  { tag8, bg1 },
  { tag9, bg1 },
};
