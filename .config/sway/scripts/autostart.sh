#!/bin/bash
# check for existing PID before executing command
function run {
  if ! pgrep -f $1 ;
  then
    $@&
  fi
}

run dbus-update-activation-environment --all
run hypridle -c ~/.config/hypr/hypridle.sway.conf
run lxpolkit
run gnome-keyring-daemon --start --components=secrets
run nm-applet
run dunst
run vesktop
run tidal-hifi
run thunar --daemon
run blueman-applet
run easyeffects --gapplication-service
run otd-daemon
run waybar -s "$HOME/.config/waybar/colorschemes/gruvbox.css" -c "$HOME/.config/waybar/config.sway.jsonc"
run virsh start idle
run clippy
