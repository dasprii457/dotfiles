#!/bin/bash
### environment variables ###
export QT_QPA_PLATFORMTHEME=qt6ct
export XDG_SCREENSHOTS_DIR=$HOME/Pictures/Screenshots
export XDG_SESSION_TYPE=wayland
export KITTY_ENABLE_WAYLAND=1
export XCURSOR_THEME=phinger-cursors-light
export XCURSOR_SIZE=24
export XDG_CURRENT_DESKTOP=sway
export XDG_SESSION_DESKTOP=sway
