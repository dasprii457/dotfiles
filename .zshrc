### dasprii's ~/.zshrc ###

### MISC ###
[[ $- != *i* ]] && return

### PROMPT ###
precmd() { print -P $'%B%F{7}%n%b%F{15}@%m %B%F{7}%~%b' }
PROMPT='%F{15}> $ '
clear-screen() { echoti clear; precmd; zle redisplay; }
zle -N clear-screen

### ENVIRONMENT VARIABLES ###
export ZSH="$HOME/.oh-my-zsh"
export EDITOR="nvim"

### PLUGINS ###
plugins=(
        git
        urltools
        zsh-autosuggestions
        zsh-syntax-highlighting
        zsh-256color
        dotbare
)

### ZSH OPTIONS ###
# Case sensitive auto completion
CASE_SENSITIVE="false"

# Automatic update configuration for oh-my-zsh (disabled, auto, reminder)
zstyle ':omz:update' mode auto

# oh-my-zsh auto update frequency (in days)
zstyle ':omz:update' frequency 1

# Autocorrect
ENABLE_CORRECTION="false"

# Prompt to show when a command is awaiting completion/is currently running
COMPLETION_WAITING_DOTS="true"

# Speeds up repository status checks for large repositories
DISABLE_UNTRACKED_FILES_DIRTY="true"

# Loads oh-my-zsh
source $ZSH/oh-my-zsh.sh

# Enables vim keybindings
bindkey -v

### ALIASES ###
alias sync="trizen -Syy"
alias update="trizen -Syyu"
alias install="trizen -S --needed"
alias remove="trizen -Rscn"
alias clean="trizen -Scc"
alias search="trizen -s --needed"
alias ls="lsd"
alias l="lsd -l"
alias la="lsd -a"
alias lla="lsd -la"
alias lt="lsd --tree"
alias ds4bat="cat /sys/class/power_supply/ps-controller-battery-1c\:a0\:b8\:46\:0c\:22/capacity"
alias startx="dbus-run-session Hyprland"
#alias startx="dbus-run-session sway"
alias vim="nvim"
alias sudo="sudo -E"
alias ports="bash $HOME/Documents/Scripts/port_forward.sh"
alias findpid="ps -aux | grep"
alias fetch="fastfetch --logo arch2"
alias neofetch="fastfetch --config neofetch"
alias gpuedit="sudo vim /etc/default/amdgpu-custom-state.card0"
#alias killwine="kill -9 $(ps aux |grep -i '\.exe' |awk '{print $2}'|tr '\n' ' ')"
alias win11="virsh start win11"
alias idleVM="virsh start idle"
alias bg="bg && disown"
alias steamscope="gamescope -W 2880 -H 1620 -r 165 -e steam"

### PATH ###
export PATH="$PATH:/home/dasprii/.local/bin"
source /usr/share/nvm/init-nvm.sh
