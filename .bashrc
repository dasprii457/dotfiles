### dasprii's  ~/.bashrc ###

### INTERACTIVE SHELL CHECK ###
if [[ $- != *i* ]] ; then
	return
fi

### PROMPT ###
PS1='\e[1;37m\u\e[0m@\h \e[1;37m\w\e[0m \n> \$ '

### ENVIRONMENT VARIABLES ###
export EDITOR="nvim"

### ALIASES ###
alias sync="trizen -Syy"
alias update="trizen -Syyu --noconfirm --noedit"
alias install="trizen -S --needed --noedit"
alias remove="trizen -Rscn"
alias clean="trizen -Scc"
alias search="trizen -s --needed --noedit"
alias ls="lsd"
alias l="lsd -l"
alias la="lsd -a"
alias lla="lsd -la"
alias lt="lsd --tree"
alias ds4bat="cat /sys/class/power_supply/ps-controller-battery-1c\:a0\:b8\:46\:0c\:22/capacity"
alias startx="echo 'preparing for lift off'; sleep 1s; echo '10'; sleep 1s; echo '9'; sleep 1s; echo '8'; sleep 1s; echo '7'; sleep 1s; echo '6'; sleep 1s; echo '5'; sleep 1s; echo '4'; sleep 1s; echo '3'; sleep 1s; echo '2'; sleep 1s; echo '1'; sleep 1s; echo 'we have liftoff'; sleep .5s; dbus-run-session Hyprland"
alias starth="dbus-run-session Hyprland"
alias shutdown="/home/dasprii/.config/hypr/scripts/virshdown.sh; shutdown"
alias reboot="/home/dasprii/.config/hypr/scripts/virshdown.sh; reboot"
alias vim="nvim"
alias sudo="sudo -E"
alias ports="bash $HOME/Documents/Scripts/port_forward.sh"
alias findpid="ps -aux | grep"
alias fetch="fastfetch --logo arch2"
alias neofetch="fastfetch --config neofetch"
alias gpuedit="sudo vim /etc/default/amdgpu-custom-state.card0"
alias killwine="kill -9 $(ps aux |grep -i '\.exe' |awk '{print $2}'|tr '\n' ' ')"
alias win11="virsh start win11"
alias idleVM="virsh start idle"
alias steamscope="gamescope -W 2560 -H 1440 -r 165 -w 2560 -h 1440 -e --adaptive-sync steam"

### PATH ###
export PATH="$PATH:/home/dasprii/.local/bin"
source /usr/share/nvm/init-nvm.sh
source ~/.dotbare/dotbare.plugin.bash

### UWSM ###
#if uwsm check may-start; then
#    exec uwsm start hyprland.desktop
#fi
