" DASPRII'S VIMRC
" -----------------------------------------------------------------------------
"  PLUGINS
" -----------------------------------------------------------------------------
" install vim-plug if it's not already installed
let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
if empty(glob(data_dir . '/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" if plugins are missing, run PlugInstall
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \| PlugInstall --sync | source $MYVIMRC
\| endif

" plugin list
call plug#begin('~/.vim/plugged')

Plug 'sheerun/vim-polyglot'

Plug 'arcticicestudio/nord-vim'

Plug 'gruvbox-community/gruvbox'

Plug 'itchyny/lightline.vim'

Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }

Plug 'sainnhe/everforest'

call plug#end()

" -----------------------------------------------------------------------------
"  PLUGIN SETTINGS
" -----------------------------------------------------------------------------
" set colorscheme for lightline
let g:lightline = { 'colorscheme': 'everforest' }
autocmd VimEnter * hi Normal ctermbg=NONE guibg=NONE
highlight Normal guibg=NONE ctermbg=NONE

" ---------------------------------------------------------------------------
" GENERAL SETTINGS
" ---------------------------------------------------------------------------

" enable syntax highlighting
syntax on

" enable hybrid line numbers
set nu rnu

" enable automatic indentation
set autoindent

" good idea to set this
set nocompatible

" disable line wrapping
set nowrap

" always display status line
set laststatus=2

" enable file recognition
filetype on

" enable file recognition for plugins
filetype plugin on

" enable file specific indentation
filetype indent on

" set the colorscheme
colorscheme everforest

" disable modeline
set noshowmode

" enables incremental search (searches as you type)
set incsearch

" disables case sensitive search
set ignorecase

" highlight matching braces
set showmatch

" highlight search patterns
set hlsearch

" dark mode colorscheme
set background=dark

" mouse cursor support
if !has('nvim')
  set mouse=nv
endif

" changes cursor to a line in insert mode
let &t_SI = "\e[5 q"
let &t_EI = "\e[2 q"

" yank to clipboard
set clipboard+=unnamedplus

" enable color support
set termguicolors

" background highlighting of hex color codes
let g:Hexokinase_highlighters = ['backgroundfull']
