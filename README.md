# dotfiles

I upload most of my configs here. They may change over time.

![](.img/hyprpanel_pretty_cool.mp4)
![](.img/2024-11-22T175955021987732-0500.png)
![](.img/2024-11-22T180457759695994-0500.png)

Screenshot Tool: [Grimblast](https://github.com/hyprwm/contrib/tree/main/grimblast)

Terminal: Kitty

OS: Arch

Font: Ubuntu Mono

Gruvbox GTK: Orchis-Grey-Dark

Gruvbox Icon Theme: Vimix Black Dark

Cursor: [Phinger](https://github.com/phisch/phinger-cursors)

WM: [Hyprland](https://hyprland.org/)

Bar: [HyprPanel](https://hyprpanel.com/) (edited via GUI. see link and related docs for info. my theme and config have been uploaded here for import)

Wallpaper: [Steam Workshop - Wise Wolf](https://steamcommunity.com/sharedfiles/filedetails/?id=1134328086)

Shell: BASH

Rofi: Taken from [this](https://github.com/adi1090x/rofi) git repo and modified to suit my preferences

I manage this dotfiles repo with [dotbare](https://github.com/kazhala/dotbare)

I use Wallpaper Engine with [linux-wallpaperengine](https://github.com/Almamu/linux-wallpaperengine)

